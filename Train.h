#ifndef _TRAIN_H_
#define _TRAIN_H_
#include <string>
namespace NTrain
{
	struct TRAIN
	{
		std::string city;
		std::string train_num;
		std::string start_time;
		
	};

	std::ostream& operator<<(std::ostream& out, const TRAIN& Train);
	std::istream& operator>>(std::istream& in, TRAIN& Train);
	std::ostream& manipStr(std::ostream& out);
	std::ostream& manipInt(std::ostream& out);
	
	void input(TRAIN* gr, int n, std::istream& in); 
	void output(const TRAIN* gr, int n, std::ostream& out);

	void findTrains(const TRAIN* gr, int n, std::string name); 
	void sortTrains(TRAIN* gr, int n);
};

#endif
