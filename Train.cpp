#include "Train.h"
#include <iostream>
#include <iomanip>
#include <sstream>
using namespace std;

namespace NTrain
{
	ostream& manipStr(ostream& out)
	{
		out << setw(15) << setfill('.')
			<< resetiosflags(ios::right)
			<< setiosflags(ios::left);
		return out;
	}
	ostream& manipInt(ostream& out)
	{
		out << setw(4) << setfill('_')
			<< resetiosflags(ios::left)
			<< setiosflags(ios::right);
		return out;
	}
	ostream& operator<<(ostream& out, const TRAIN& stud)
	{
		out << manipStr << stud.city
			<< manipStr << stud.train_num
			<< manipStr << stud.start_time;
			return out;
	}
	istream& operator>>(istream& in, TRAIN& stud)
	{
		getline(in, stud.city, ';');
		getline(in, stud.train_num, ';');
		getline(in, stud.start_time);
		
		return in;
	}

	
	void input(TRAIN* gr, int n, std::istream& in) 
	{
		int i = 0;
		while (i < n && !in.eof())
		{
			in >> gr[i++];
		}
	}
	void output(const TRAIN* gr, int n, std::ostream& out) 
	{
		for (int i = 0; i < n; ++i)
			out << gr[i]<<endl;
	}
	
	void findTrains(const TRAIN* gr, int n, std::string name)
	{
		const TRAIN* result = NULL;
		cout << "\t Your trains"<<endl;
		bool trains_find = false;
		for (int i = 0; i < n; ++i)
		{
			if (gr[i].city.find(name) != std::string::npos)
			{
				result = &gr[i];
				cout << gr[i]<<endl;

			}
		}
		if (!trains_find) {
			cout << "Trains not found" << endl;;
		}
		
	
	}
	void sortTrains(TRAIN* gr, int n) {
		for (int i = 0; i < n;i++) {
			for (int j = 0;j < n-1;j++) {
				char* s1 = const_cast<char*>(gr[j].city.c_str());
				char* s2 = const_cast<char*>(gr[j+1].city.c_str());			

				if (strcmp(s1, s2) > 0) {
					TRAIN tmp = gr[j];
					gr[j] = gr[j+1];
					gr[j+1] = tmp;
				}
				

			}
		}
	}



}