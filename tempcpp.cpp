// tempcpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

// swapping ostringstream objects
#include <string>       // std::string
#include <iostream>     // std::cout
#include<vector>
#include <sstream>      // std::stringstream
using namespace std;


vector<string> data_from_stream(stringstream* input);

int main() {

	stringstream ss;

	ss << "Ivanov" << ' ' << "�������";
	vector<string> data = data_from_stream(&ss);
	cout << data[0];
	return 0;
}

vector<string> data_from_stream(stringstream* input) {
	vector<string> out_array;
	while ((*input))
	{
		string s;
		(*input) >> s;
		out_array.push_back(s);
	}
	return out_array;
}