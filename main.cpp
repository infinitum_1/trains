#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <sstream>
#include <string>
#include <fstream>
#include "Train.h"
using namespace std;

int countString(istream& in)
{
	int count = 0;
	std::string line;
	while (!in.eof())
	{
		std::getline(in, line);
		if (in && !line.empty()) ++count;
	}
	return count;
}
int main()
{
	std::string inputDataStr = "B;1;08.03.2020 17:30\nCccs;2;08.04.2020 17:30\nADD;3;08.05.2020 17:30\nZ4;3;08.05.2020 18:30\nAv;3;08.05.2020 18:30";
	std::istringstream inputData(inputDataStr);
	
	int n = countString(inputData);
	NTrain::TRAIN* mas = new NTrain::TRAIN[n];
	

	
	inputData.seekg(0);
	inputData.clear(); 
	
	std::ostringstream resultData;
	
	NTrain::input(mas, n, inputData);
	NTrain::sortTrains(mas, n);
	NTrain::output(mas, n, resultData);

	std::ofstream fout("inputdata.txt");
	cout << resultData.str() << endl;
	fout << resultData.str() << endl;
	fout.close();
	
	resultData.str("");
	
	while (true)
	{
		string input_command;
		cout << "Enter train name: ";
		cin >> input_command;

		if (input_command == "$exit")
			break;

		NTrain::findTrains(mas, n, input_command);

	}

	delete[] mas;
	

	system("pause");
	return 0;
}